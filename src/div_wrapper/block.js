/**
 * BLOCK: Div wrapper where we can nest other blocks
 *
 */

import './editor.scss';

const { registerBlockType } = wp.blocks;
const { InnerBlocks } = wp.editor;

registerBlockType( 'cgb/div-wrapper', {
    title: 'Div Wrapper',
    
    icon: 'editor-code',

    category: 'layout',

    supports: {
		anchor: true,
	},

	edit( { className } ) {
		return (
			<div className={ className }>
				<InnerBlocks />
			</div>
		);
	},

	save( { className } ) {
		return (
			<div className={ className }>
				<InnerBlocks.Content />
			</div>
		);
	}
} );