const { registerBlockType } = wp.blocks;
const { RichText } = wp.editor;

registerBlockType( 'cgb/card', {
    title: 'Card',

    icon: 'smiley',

    category: 'layout',

    attributes: {
		title: {
			type: 'array',
			source: 'children',
			selector: 'h2',
		},
        content: {
            type: 'string',
            source: 'html',
            selector: 'p',
        },
    },

    edit( { attributes, className, setAttributes } ) {
        const { title, content } = attributes;

		function onChangeTitle( value ) {
			setAttributes( { title: value } );
		}
        function onChangeContent( newContent ) {
            setAttributes( { content: newContent } );
        }

        return (
			<div className = { className }>
				<RichText
					tagName="h2"
					className="card-title"
					onChange={ onChangeTitle }
					value={ title }
					placeholder="Card Title"
				/>
				<RichText
					tagName="p"
					className="card-text"
					onChange={ onChangeContent }
					value={ content }
					placeholder="Card Body"
				/>
			</div>
        );
    },

    save( { attributes, className } ) {
        const { title, content } = attributes;

        return (
			<div className = { className }>
				<RichText.Content
					tagName="h2"
					className="card-title"
					value={ title }
				/>
				<RichText.Content
					tagName="p"
					className="card-text"
					value={ content }
				/>
			</div>
        );
    },
} );